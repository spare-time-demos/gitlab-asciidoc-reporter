package org.manathome.gradle.gitlabreporter.core;

import org.manathome.gradle.gitlabreporter.model.GitlabIssue;

/** available tags in changelog.peb template. */
public final class TemplateFieldTags {
	
	/** generate changelog with caption/headers. true/false. */
	public static final String withHeaders = "withHeaders";

	public static final String issuesByMilestone = "issuesByMilestone";

	/** list of {@link GitlabIssue}. */
	public static final String issues = "issues";
}

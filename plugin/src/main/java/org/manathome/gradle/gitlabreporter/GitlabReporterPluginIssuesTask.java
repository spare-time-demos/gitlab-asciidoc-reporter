package org.manathome.gradle.gitlabreporter;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;

import java.io.File;

/** gradle build task. */
public class GitlabReporterPluginIssuesTask extends DefaultTask {

	/** gradle task name. */
	public static final String GITLAB_ISSUES_TASK_NAME = "gitlabIssues";

	private File markupFile = null;

	private boolean withHeaders = true;

	private String gitlabRestBaseUrl;

	private String projectId;

	@Input
	public String getProjectId() {
		return projectId;
	}

	/** gitlab project name. */
	public void setProjectId(final String projectId) {
		this.projectId = projectId;
	}

	@Input
	public String getGitlabRestBaseUrl() {
		return gitlabRestBaseUrl;
	}

	/** rest api base url (will be used for data retrieval). */
	public void setGitlabRestBaseUrl(final String gitlabRestBaseUrl) {
		this.gitlabRestBaseUrl = gitlabRestBaseUrl;
	}

	@OutputFile
	public File getMarkupFile() {
		return markupFile;
	}

	/** output asciidoc changelog file. */
	public void setMarkupFile(File markupFile) {
		this.markupFile = markupFile;
	}

	@Input
	public boolean isWithHeaders() {
		return withHeaders;
	}

	/** generate a level 1 caption "changelog". */
	public void setWithHeaders(boolean withHeaders) {
		this.withHeaders = withHeaders;
	}

	/** ctor. */
	public GitlabReporterPluginIssuesTask() {

		super();
		setGroup("documentation");
		setDescription("generate an asciidoc markup file with gitlab issues information.");

		this.getLogger().debug("ctor " + GITLAB_ISSUES_TASK_NAME);
	}

	/** parse and generate. 
	 * 
	 * @throws Exception (io, authentication...)
	 * */
	@TaskAction
	public void gitlabIssuesGenerate() throws Exception {
		
		this.getLogger().info("generating issues file ..: " + this.getMarkupFile().getCanonicalFile());
		
		GitlabProcessor processor = new GitlabProcessor(
						getGitlabRestBaseUrl(),
						getProjectId());

		processor.processIssues(
				this.getMarkupFile(),
				this.withHeaders);
	}

}

package org.manathome.gradle.gitlabreporter;

/** environment variable used. */
public class PluginEnvironmentVariables {

	/** environment variable, secret gitlab api access token. */
	public static final String GITLAB_ACCESS_TOKEN = "GITLAB_ACCESS_TOKEN";
}

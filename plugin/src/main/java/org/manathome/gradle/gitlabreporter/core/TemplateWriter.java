package org.manathome.gradle.gitlabreporter.core;

import io.pebbletemplates.pebble.PebbleEngine;
import io.pebbletemplates.pebble.template.PebbleTemplate;
import org.manathome.gradle.gitlabreporter.model.GitlabIssue;
import org.manathome.gradle.gitlabreporter.model.GitlabMilestone;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/** use pebble template to generate asciidoc output from jira issues. */
public class TemplateWriter {
	
	private final PebbleTemplate template;

	/** create from template.
	 * @param templatePath pebble markup file*/
	public TemplateWriter(final String templatePath) {
		final PebbleEngine engine = new PebbleEngine.Builder().build();
		this.template = engine.getTemplate(templatePath);		
	}

	/** generate markup from template and input objects.
	 * @param issues list of gitlab issues
	 * @param withHeaders add asciidoc header in output
	 * @return asciidoc markup string
	 * @throws IOException on markup creation */
	public String generate(
					final Map<GitlabMilestone, List<GitlabIssue>> issuesByMilestone,
					final List<GitlabIssue> issues,
					boolean withHeaders) throws IOException {
				
		try(Writer writer = new StringWriter()) {

			Map<String, Object> context = new HashMap<>();

			context.put(TemplateFieldTags.issuesByMilestone, issuesByMilestone);
			context.put(TemplateFieldTags.issues, issues);
			context.put(TemplateFieldTags.withHeaders, withHeaders);
			template.evaluate(writer, context);

			return writer.toString();
		}
	}

}

package org.manathome.gradle.gitlabreporter.core;

import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Stream;

import org.manathome.gradle.gitlabreporter.PluginEnvironmentVariables;
import org.manathome.gradle.gitlabreporter.model.GitlabIssue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class GitlabIssueReader {

  private static final Logger logger = LoggerFactory.getLogger(GitlabIssueReader.class);

  private final ObjectMapper om = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

  private final String gitlabRestUrl;

  public GitlabIssueReader(final String gitlabRestUrl) {

    if (gitlabRestUrl == null || gitlabRestUrl.length() <= 5) {
      throw new RuntimeException("no GITLAB REST URL given");
    }

    this.gitlabRestUrl = gitlabRestUrl;
  }

  /**
   * get data via rest api from gitlab.
   */
  public Stream<GitlabIssue> retrieveClosedIssues(final String projectId) throws IOException {

    final String gitlabApiToken = System.getenv(PluginEnvironmentVariables.GITLAB_ACCESS_TOKEN);
    if (gitlabApiToken == null || gitlabApiToken.length() <= 2) {
      throw new RuntimeException("no api token found with System.getenv "
              + PluginEnvironmentVariables.GITLAB_ACCESS_TOKEN);
    }
    if (projectId == null || projectId.length() == 0) {
      throw new RuntimeException("no project id given");
    }

    // 	"summary", "status", "labels", "fixVersions"

    Request request = new Request.Builder()
            .url(gitlabRestUrl + "/projects/" + projectId + "/issues")
            .get()
            .addHeader("content-type", "application/json")
            .addHeader("User-Agent", "gradleplugin/gitlab-asciidoc-reader/OkHttp")
            .addHeader("PRIVATE-TOKEN", gitlabApiToken)
            .build();

    try (Response response = new OkHttpClient().newCall(request).execute()) {
      defaultValidateOk(response);
      return mapResponseToIssues(response.body().string());
    }

  }

  /**
   * convert json response to gitlab issues value objects.
   */
  public Stream<GitlabIssue> mapResponseToIssues(final String jsonResponse) {

    try {

      GitlabIssue[] issues = om.readValue(jsonResponse, GitlabIssue[].class);
      return Arrays.stream(issues);

    } catch (Exception e) {
      logger.error("error reading json", e);
      throw new RuntimeException("error reading json" + e.getMessage(), e);
    }
  }

  private void defaultValidateOk(final Response response) {

    if (response == null) throw new RuntimeException("no response");
    if (!response.isSuccessful())
      throw new RuntimeException("error rest call: " + response.message() + response.code());
  }

}

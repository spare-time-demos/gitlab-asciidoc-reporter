package org.manathome.gradle.gitlabreporter;

import java.io.File;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;

/** gradle task, generating a changelog.adoc file. */
public class GitlabReporterPluginChangelogTask extends DefaultTask {

	/** gradle task name. */
	public static final String GITLAB_CHANGELOG_TASK_NAME = "gitlabChangelog";

	private File markupFile = null;
	private boolean withHeaders = true;
	private String gitlabRestBaseUrl;
	private String projectId;


	/** gitlab project id. */
	@Input
	public String getProjectId() {
		return projectId;
	}

	/** gitlab project id. */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	/** rest api base url (will be used for data retrieval). */
	@Input
	public String getGitlabRestBaseUrl() {
		return gitlabRestBaseUrl;
	}

	/** rest api base url (will be used for data retrieval). */
	public void setGitlabRestBaseUrl(final String gitlabRestBaseUrl) {
		this.gitlabRestBaseUrl = gitlabRestBaseUrl;
	}

	/** output asciidoc changelog file. */
	@OutputFile
	public File getMarkupFile() {
		return markupFile;
	}

	/** output asciidoc changelog file. */
	public void setMarkupFile(File markupFile) {
		this.markupFile = markupFile;
	}

	/** generate a level 1 caption "changelog". */
	@Input
	public boolean isWithHeaders() {
		return withHeaders;
	}

	/** generate a level 1 caption "changelog". */
	public void setWithHeaders(boolean withHeaders) {
		this.withHeaders = withHeaders;
	}

	/** ctor. */
	public GitlabReporterPluginChangelogTask() {
		super();
		setGroup("documentation");
		setDescription("Parses java source files and produce asciidoc markup.");

		this.getLogger().debug("ctor " + GITLAB_CHANGELOG_TASK_NAME);
	}

	/** parse and generate "gitlabChangelog" task
	 * 
	 * @throws Exception (io, authentication...)
	 * */
	@TaskAction
	public void gitlabChangelogGenerate() throws Exception {

		this.getLogger().info("generating changelog file ..: " + this.getMarkupFile().getCanonicalFile());

		GitlabProcessor processor = new GitlabProcessor(
						getGitlabRestBaseUrl(),
						getProjectId());

		processor.processChangelog(
						this.getMarkupFile(),
						this.withHeaders);
	}


	
}

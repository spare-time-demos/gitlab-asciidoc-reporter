package org.manathome.gradle.gitlabreporter;

import org.gradle.api.Project;
import org.gradle.api.Plugin;

/**
 * gitlab reporter plugin: a markup generator gradle plugin.
 */
public class GitlabReporterPlugin implements Plugin<Project> {

	/** gitlab plugin id. */
  public static final String GITLABREPORTER_PLUGIN_ID =  "io.github.man-at-home.gitlabreporter";

  @Override
	public void apply(Project project) {
    	
		project.getLogger().debug("apply gitlabreporter plugin.");
					    
		project.getTasks().register(
	    		GitlabReporterPluginChangelogTask.GITLAB_CHANGELOG_TASK_NAME,
	    		GitlabReporterPluginChangelogTask.class);

		project.getTasks().register(
						GitlabReporterPluginIssuesTask.GITLAB_ISSUES_TASK_NAME,
						GitlabReporterPluginIssuesTask.class);
	}
    
}

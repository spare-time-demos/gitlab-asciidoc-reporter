package org.manathome.gradle.gitlabreporter.model;

import java.util.Date;
import java.util.Optional;

/** gitlab issue json subset. */
public class GitlabIssue {

	/** internal technical id. */
	public String id;

	/** acutal public id. */
	public String iid;

	/** main issue title/subject. */
	public String title;

	/** open, closed..*/
	public String state;

	/** issue, .. */
	public String type;

	public String description;

	public Date created_at;

	public Date closed_at;

	/** url to gitlab issue an gitlab server. */
	public String web_url;

	/** optional labels. */
	public String[] labels;

	/** optional milestone. */
	public GitlabMilestone milestone;

	public GitlabMilestone getMilestone() {
		return milestone;
	}
}

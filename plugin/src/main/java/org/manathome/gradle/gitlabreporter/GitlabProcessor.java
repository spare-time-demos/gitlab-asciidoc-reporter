package org.manathome.gradle.gitlabreporter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.manathome.gradle.gitlabreporter.core.GitlabIssueReader;
import org.manathome.gradle.gitlabreporter.core.TemplateWriter;
import org.manathome.gradle.gitlabreporter.model.GitlabIssue;
import org.manathome.gradle.gitlabreporter.model.GitlabMilestone;

/**
 * core functionality of plugin.
 */
public class GitlabProcessor {

  protected final GitlabIssueReader reader;
  protected final String projectId;

  public GitlabProcessor(
          final String gitlabRestBaseUrl,
          final String projectId) {
    this.reader = new GitlabIssueReader(gitlabRestBaseUrl);
    this.projectId = projectId;
  }

  public String processIssues(final File output, boolean withHeaders) throws FileNotFoundException, IOException {

    if (output == null) {
      throw new NullPointerException("output file argument null.");
    }

    final Stream<GitlabIssue> issues = retrieveClosedIssues(this.projectId);

    final TemplateWriter template = new TemplateWriter("templates/issues.peb");

    final String markup = template.generate(
            null,
            issues.collect(Collectors.toList()),
            withHeaders);

    try (PrintWriter out = new PrintWriter(output)) {
      out.print(markup);
    }

    return markup;
  }

  public String processChangelog(final File output, boolean withHeaders) throws FileNotFoundException, IOException {

    if (output == null) {
      throw new NullPointerException("output file argument null.");
    }

    final GitlabMilestone emptyMilestone = new GitlabMilestone();
    emptyMilestone.id = 0;
    emptyMilestone.title = "no milestone";
    emptyMilestone.description = "issues without a milestone";

    final TemplateWriter template = new TemplateWriter("templates/changelog.peb");
    final List<GitlabIssue> issues = retrieveClosedIssues(this.projectId).collect(Collectors.toList());

    final Map<GitlabMilestone, List<GitlabIssue>> issuesByMilestone = issues
            .stream()
            .collect(Collectors.groupingBy(issue -> issue.getMilestone() == null
                    ? emptyMilestone
                    : issue.getMilestone())
            );

    final String markup = template.generate(
            issuesByMilestone,
            issues,
            withHeaders);

    try (PrintWriter out = new PrintWriter(output)) {
      out.print(markup);
    }

    return markup;
  }

  public Stream<GitlabIssue> retrieveClosedIssues(final String projectId) throws IOException {

    return this.reader.retrieveClosedIssues(projectId);
  }

}

package org.manathome.gradle.gitlabreporter.model;

import java.util.Date;

/** gitlab milestone.*/
public class GitlabMilestone {

  /** internal id. */
  public long id;

  /** milestone name. */
  public String title;
  public String description;
  public Date start_date;
  public Date due_date;
  public String web_url;
  public String state;

  @Override
  public int hashCode() {
    return Long.hashCode(id);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    GitlabMilestone that = (GitlabMilestone) o;
    return id == that.id;
  }
}

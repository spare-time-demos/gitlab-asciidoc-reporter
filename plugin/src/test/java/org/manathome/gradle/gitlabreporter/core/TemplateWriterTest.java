package org.manathome.gradle.gitlabreporter.core;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.manathome.gradle.gitlabreporter.model.GitlabIssue;

public class TemplateWriterTest {

	private final TemplateWriter template = new TemplateWriter("templates/issues.peb");

	static List<GitlabIssue> buildIssues() {

		List<GitlabIssue> issues = new ArrayList<>();
		
		for(int i = 0; i < 5; i++) {
			var issue = new GitlabIssue();
			issue.id = "" + i++;
			issue.state = "closed";
			issue.title = "a issue of " + i;
			issue.labels = new String[] {"documentation", "technical"};

			issues.add(issue);
		}

		return issues;
	}

	@Test
	void test_fill_issues_template() throws Exception {

		  var issues = buildIssues();
			var doc = template.generate(null, buildIssues(),true);

			assertThat(doc).contains("= list of issues");
			assertThat(doc).contains(issues.get(0).title);
			assertThat(doc).contains(issues.get(1).state);
			assertThat(doc).contains(issues.get(2).labels[0]);
	}

}

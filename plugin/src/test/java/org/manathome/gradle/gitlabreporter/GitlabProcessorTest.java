package org.manathome.gradle.gitlabreporter;

import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.nio.file.Files;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;

public class GitlabProcessorTest {

  private final File output = new File("build/gitlab_output.adoc");

  private WireMockServer wireMockServer;

  private GitlabProcessor processor;

  @BeforeEach
  public void beforeEach() {
    wireMockServer = new WireMockServer(options()
            .dynamicPort()
            .usingFilesUnderDirectory("src/test/resources/json"));

    wireMockServer.start();

    wireMockServer
            .stubFor(get(WireMock.anyUrl())
                    .willReturn(WireMock.aResponse().withHeader("Content-Type", "text/json")
                            .withStatus(200)
                            .withBodyFile("issues.json")));

    if (output.exists()) {
      output.delete();
    }

    processor = new GitlabProcessor(
            wireMockServer.baseUrl() + "/rest/api/v3",
            PluginTestConstants.projectId);

  }

  @AfterEach
  public void afterEach() {
    wireMockServer.stop();
  }

  @Test
  public void process_GitlabToMarkupFile_Issues_Ok() throws Exception {


    assertThat(wireMockServer.baseUrl()).startsWith("http");

    processor.processIssues(output, true);

    assertThat(output.exists()).as(output.getAbsolutePath() + " found").isTrue();
    String content = Files.readString(output.toPath());

    System.out.println("-------------------------");
    System.out.println(content);
    System.out.println("-------------------------");

    assertThat(content).as("correct header").contains("= list of issues");
    assertThat(content).as("an issue title").contains("add sonarcloud scan for ftclient");
  }

  @Test
  public void process_GitlabToMarkupFile_Changelog_Ok() throws Exception {

    processor.processChangelog(output, true);

    assertThat(output.exists()).as(output.getAbsolutePath() + " found").isTrue();
    String content = Files.readString(output.toPath());

    System.out.println("-------------------------");
    System.out.println(content);
    System.out.println("-------------------------");

    assertThat(content).as("correct header").contains("= changelog");
    assertThat(content).as("no milestone section").contains("== no milestone");
    assertThat(content)
            .as("milestone section")
            .contains("== https://gitlab.com/spare-time-demos/freetime/-/milestones/2[v0.2_project_infrastructure_in_place] \n");

    assertThat(content)
            .as("issue list item")
            .contains("* https://gitlab.com/spare-time-demos/freetime/-/issues/29[29] connect external planning tool *closed*\n");
  }


  @Test
  public void process_RequiresProjectId_Nok() throws Exception {

    assertThrows(RuntimeException.class, () -> {
      GitlabProcessor p = new GitlabProcessor(
              wireMockServer.baseUrl() + "/rest/api/v4/wrongcall",
              null);

      p.processIssues(output, true);
    });
  }

  @Test
  public void process_RequiresRestUrl_Nok() {

    assertThrows(RuntimeException.class, () -> {
      new GitlabProcessor(" ", PluginTestConstants.projectId);
    });
  }

}

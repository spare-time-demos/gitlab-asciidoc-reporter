package org.manathome.gradle.gitlabreporter;

public class PluginTestConstants {

	public static final String gitlabRestBaseUrl = "https://gitlab.com/api/v4/";
	public static final String jiraWebUrl  = "https://man-at-home.atlassian.net";
	public static final String projectId = "42602329";

}

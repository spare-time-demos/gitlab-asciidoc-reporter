package org.manathome.gradle.gitlabreporter.core;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.manathome.gradle.gitlabreporter.PluginTestConstants;
import org.manathome.gradle.gitlabreporter.model.GitlabIssue;

/** test deserialization gitlab issue json. */
public class GitlabIssueReaderTest {
	
	private final GitlabIssueReader reader = new GitlabIssueReader(PluginTestConstants.gitlabRestBaseUrl);
	
	static String readFromFile() throws IOException {
		
		final Path p = Paths.get("src/test/resources/json/__files/issues.json");		
		return Files.readString(p);
	}

	@Test
	public void test_ReadFromJsonResult_ToGitlabIssue() throws Exception {
		String s = readFromFile();
		Stream<GitlabIssue> issues = reader.mapResponseToIssues(s);
		List<GitlabIssue> i = issues.toList();
		
		// will fail on new jira issues in json, adapt in this case:
		assertThat(i.size()).as("13 gitlab issue in file expected").isEqualTo(13);
	}

	@Test
	public void test_FirstIssueData_FromJson() throws Exception {
		String s = readFromFile();
		Stream<GitlabIssue> issues = reader.mapResponseToIssues(s);
		List<GitlabIssue> i = issues.toList();

		Date startDate = new GregorianCalendar(2023, Calendar.MARCH, 5).getTime();

		GitlabIssue issue = i.get(0);
		assertThat(issue.id).as("first issue id in file unexpected").isEqualTo("124750371");
		assertThat(issue.state).as("opened state expected").isEqualTo("opened");
		assertThat(issue.title).as("unexpected issue title").contains("generated from gitlab");
		assertThat(issue.description).as("description").contains("ChangeLogGenerator");
		assertThat(issue.labels).isNotNull();
		assertThat(issue.labels[0]).as("documentation label expected").contains("documentation");
		assertThat(issue.type).isEqualTo("ISSUE");
		assertThat(issue.web_url).startsWith("http");
		assertThat(issue.created_at).as("created_at").isBetween(startDate, new Date());
		assertThat(issue.closed_at).isNull();
	}

	@Test
	public void test_SecondIssue_MilestoneData_FromJson() throws Exception {
		String s = readFromFile();
		Stream<GitlabIssue> issues = reader.mapResponseToIssues(s);
		List<GitlabIssue> i = issues.toList();

		GitlabIssue issue = i.get(1);
		assertThat(issue.id).as("second issue id in file unexpected").isEqualTo("124585576");
		assertThat(issue.milestone).isNotNull();
		assertThat(issue.milestone.title).contains("v0.2_project_infrastructure_in_place");
		assertThat(issue.milestone.state).contains("active");
	}

}

package org.manathome.gradle.gitlabreporter.core;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.manathome.gradle.gitlabreporter.PluginTestConstants;
import org.manathome.gradle.gitlabreporter.model.GitlabIssue;

/** access actual gitlab instance for a project, may fail whenever gitlab server is down. */
public class GitlabIssueReaderIntegrationTest {
	
	private GitlabIssueReader reader = null;

	@BeforeEach
	void beforeEach() {
		reader = new GitlabIssueReader(PluginTestConstants.gitlabRestBaseUrl);
	}

	@Test
	void readIssues_FromGitlabCloud() throws IOException {

		final Stream<GitlabIssue> issues = reader.retrieveClosedIssues(PluginTestConstants.projectId);
		assertThat(issues).as("issues returned").isNotNull();
	}

	@Test
	void readAndCheck_IssuesFound_FromGitlabCloud() throws IOException {

		final Stream<GitlabIssue> issues = reader.retrieveClosedIssues(PluginTestConstants.projectId);
		List<GitlabIssue> i = issues.toList();
		assertThat(i.size()).as("issues found").isGreaterThanOrEqualTo(3);
	}

	@Test
	void readAndCheck_Content_FirstIssues_FromGitlabCloud() throws IOException {

		final Stream<GitlabIssue> issues = reader.retrieveClosedIssues(PluginTestConstants.projectId);
		List<GitlabIssue> i = issues.toList();
		GitlabIssue issue = i.get(0);

		assertThat(issue.iid).isNotEmpty();
		assertThat(issue.web_url).endsWith(issue.iid);
		assertThat(issue.title).isNotEmpty();
	}

}

package org.manathome.gradle.gitlabreporter;

import static org.assertj.core.api.Assertions.assertThat;  

import org.gradle.testfixtures.ProjectBuilder;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

/**
 * A simple unit test for plugin.
 */
@Tag("plugin")
public class GitlabReporterPluginTest {
	
    @Test 
    public void plugin_RegistersIts_IssuesTask() {
        assertThat(getTaskForTest(GitlabReporterPluginIssuesTask.GITLAB_ISSUES_TASK_NAME))
        .isNotNull();
    }

    @Test
    public void plugin_RegistersIts_ChangelogTask() {
        assertThat(getTaskForTest(GitlabReporterPluginChangelogTask.GITLAB_CHANGELOG_TASK_NAME))
                .isNotNull();
    }

    private Task getTaskForTest(final String taskName) {

        Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply(GitlabReporterPlugin.GITLABREPORTER_PLUGIN_ID);

        return project
                .getTasks()
                .findByName(taskName);
    }
}
